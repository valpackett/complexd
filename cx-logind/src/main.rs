#![feature(auto_traits, negative_impls)]

mod db;

use smol::{block_on, fs, lock::RwLock, spawn};
use std::{
    convert::{TryFrom, TryInto},
    error::Error as StdError,
    sync::Arc,
};
use zbus::{fdo, zvariant};
use zbus_polkit::policykit1 as pk;

#[derive(zbus::DBusError, Debug)]
#[dbus_error(prefix = "org.freedesktop.login1")]
enum Error {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    InternalError(String),
    NotPermitted,
    NoSenderUniqueID,
    ControllerAlreadyExists,

    // These are from logind
    NoSuchSession,
    NoSessionForPID,
    NoSuchUser,
    NoUserForPID,
    NoSuchSeat,
    SessionNotOnSeat,
    NotInControl,
    DeviceIsTaken,
    DeviceNotTaken,
    OperationInProgress,
    SleepVerbNotSupported,
}

// damn overlap with the trivial From impl >_<
auto trait NotOurError {}
impl !NotOurError for Error {}

impl<E> From<E> for Error
where
    E: StdError + NotOurError + Send + Sync + 'static,
{
    fn from(error: E) -> Self {
        Error::InternalError(error.to_string())
    }
}

type Result<T> = std::result::Result<T, Error>;

// object_server//     .remove::<Session, _>(format!("/org/freedesktop/login1/session/{}", sid))
//     .await?;

#[derive(Clone)]
struct Ctx {
    auth: pk::AuthorityProxy<'static>,
    db: Arc<RwLock<db::Db>>,
}

struct Seat {
    ctx: Ctx,
    // name: String, // we don't support multiseat (yet) so always seat0
}

#[zbus::dbus_interface(name = "org.freedesktop.login1.Seat")]
impl Seat {
    async fn terminate(&self) {
        // all sessions on seat
    }
    async fn activate_session(&self, s: String) {}
    async fn switch_to(&self, vtnr: u32) {}
    async fn switch_to_next(&self) {}
    async fn switch_to_previous(&self) {}
}

struct User {
    ctx: Ctx,
    uid: u32,
}

#[zbus::dbus_interface(name = "org.freedesktop.login1.User")]
impl User {}

struct Session {
    ctx: Ctx,
    sid: String,
}

#[zbus::dbus_interface(name = "org.freedesktop.login1.Session")]
impl Session {
    async fn take_control(
        &self,
        #[zbus(connection)] conn: &zbus::Connection,
        #[zbus(header)] msg_hdr: zbus::MessageHeader<'_>,
        force: bool,
    ) -> Result<()> {
        let sender = msg_hdr.sender()?.ok_or(Error::NoSenderUniqueID)?.to_owned();
        eprintln!("{:?}", sender);
        let mut db = self.ctx.db.write().await;
        let session = db.sessions.get_mut(&self.sid).ok_or(Error::NoSuchSession)?;
        let user = cxutil::bus_peer_uid(conn, sender.clone()).await.unwrap();
        if user != 0 && user != session.uid {
            return Err(Error::NotPermitted);
        }
        if session.controller.is_some() && !(user == 0 && force) {
            return Err(Error::ControllerAlreadyExists);
        }
        session.controller = Some(sender);
        // TODO: monitor the connection for close, implicitly release control
        eprintln!("{:?}", user);
        Ok(())
    }

    async fn release_control(&self, #[zbus(header)] msg_hdr: zbus::MessageHeader<'_>) -> Result<()> {
        let mut db = self.ctx.db.write().await;
        let session = db.sessions.get_mut(&self.sid).ok_or(Error::NoSuchSession)?;
        if session.controller.as_ref() != msg_hdr.sender()? {
            return Err(Error::NotInControl);
        }
        session.controller = None;
        Ok(())
    }

    async fn set_type(&self, #[zbus(header)] msg_hdr: zbus::MessageHeader<'_>, typ: String) -> Result<()> {
        let mut db = self.ctx.db.write().await;
        let session = db.sessions.get_mut(&self.sid).ok_or(Error::NoSuchSession)?;
        if session.controller.as_ref() != msg_hdr.sender()? {
            return Err(Error::NotInControl);
        }
        session.typ = typ;
        // TODO: reset when control is released
        Ok(())
    }
}

struct Manager {
    ctx: Ctx,
}

#[zbus::dbus_interface(name = "org.freedesktop.login1.Manager")]
impl Manager {
    #[dbus_interface(out_args("object_path"))]
    async fn get_session(&self, session_id: String) -> Result<(zvariant::OwnedObjectPath,)> {
        if !self.ctx.db.read().await.sessions.contains_key(&session_id) {
            return Err(Error::NoSuchSession);
        }
        Ok((zvariant::OwnedObjectPath::try_from(format!("/org/freedesktop/login1/session/{}", session_id)).unwrap(),))
    }

    #[dbus_interface(out_args("object_path"))]
    async fn get_user(&self, uid: u32) -> Result<(zvariant::OwnedObjectPath,)> {
        // TODO check
        Ok((zvariant::OwnedObjectPath::try_from(format!("/org/freedesktop/login1/user/{}", uid)).unwrap(),))
    }

    #[dbus_interface(out_args("object_path"))]
    fn get_seat(&self, seat_id: String) -> Result<(zvariant::OwnedObjectPath,)> {
        if seat_id != "seat0" {
            return Err(Error::NoSuchSeat);
        }
        Ok((zvariant::OwnedObjectPath::try_from(format!("/org/freedesktop/login1/seat/{}", seat_id)).unwrap(),))
    }

    // 1,1,"","","","","",1,"","",False,"","",[]
    #[dbus_interface(out_args(
        "session_id",
        "object_path",
        "runtime_path",
        "fifo_fd",
        "uid",
        "seat_id",
        "vtnr",
        "existing"
    ))]
    async fn create_session(
        &self,
        #[zbus(signal_context)] sig_ctx: zbus::SignalContext<'_>,
        #[zbus(object_server)] object_server: &zbus::ObjectServer,
        uid: u32,
        _pid: u32, // not used?
        service: String,
        typ: String,
        class: String,
        desktop: String,
        seat_id: String,
        vtnr: u32,
        tty: String,
        display: String,
        remote: bool,
        remote_user: String,
        remote_host: String,
        _properties: Vec<(&str, zvariant::Value<'_>)>, // for compat only, unused by us currently
    ) -> Result<(
        String,
        zvariant::OwnedObjectPath,
        String,
        zvariant::Fd,
        u32,
        String,
        u32,
        bool,
    )> {
        let sid = nano_id::base62::<16>();
        let (pipe_ours, pipe_theirs) = nix::unistd::pipe()?;
        {
            let mut db = self.ctx.db.write().await;
            db.sessions.insert(
                sid.clone(),
                db::Session {
                    uid,
                    controller: None,
                    pipe: pipe_ours,
                    typ,
                },
            );
        }
        let session_path = zvariant::OwnedObjectPath::try_from(format!("/org/freedesktop/login1/session/{}", sid)).unwrap();
        object_server
            .at(
                session_path.clone(),
                Session {
                    ctx: self.ctx.clone(),
                    sid: sid.clone(),
                },
            )
            .await?;
        Ok((
            sid,                             // session_id
            session_path,                    // object_path
            desktop,                         // runtime_path
            zvariant::Fd::from(pipe_theirs), // fifo_fd
            uid,                             // uid
            seat_id,                         // seat_id
            vtnr,                            // vtnr
            false,                           // existing
        ))
    }
}

fn main() -> std::result::Result<(), Box<dyn StdError>> {
    #[cfg(target_os = "freebsd")]
    if let Err(e) = cxutil::set_priority_realtime() {
        eprintln!("Failed realtime: {:?}", e);
    }
    #[cfg(target_os = "freebsd")]
    if let Err(e) = cxutil::set_process_essential() {
        eprintln!("Failed essential: {:?}", e);
    }

    block_on(async {
        let conn = zbus::ConnectionBuilder::system()?.internal_executor(false).build().await?;
        let con = conn.clone();
        spawn(async move {
            loop {
                con.executor().tick().await
            }
        })
        .detach();
        let ctx = Ctx {
            auth: pk::AuthorityProxy::new(&conn).await?,
            db: Arc::new(RwLock::new(db::Db::default())),
        };
        let object_server = conn.object_server();
        object_server
            .at("/org/freedesktop/login1", Manager { ctx: ctx.clone() })
            .await?;
        object_server
            .at("/org/freedesktop/login1/seat/seat0", Seat { ctx: ctx.clone() })
            .await?;
        conn.request_name("org.freedesktop.login1").await?;
        loop {
            std::thread::park();
        }
    })
}
