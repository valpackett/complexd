use std::{collections::HashMap, os::unix::io::RawFd};

pub struct Session {
    pub uid: u32,
    pub controller: Option<zbus::names::UniqueName<'static>>,
    pub pipe: RawFd,
    pub typ: String,
}

#[derive(Default)]
pub struct Db {
    pub sessions: HashMap<String, Session>,
}
