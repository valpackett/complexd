<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC "-//freedesktop//DTD polkit Policy Configuration 1.0//EN" "http://www.freedesktop.org/software/polkit/policyconfig-1.dtd">
<policyconfig>

	<vendor>complexd</vendor>
	<vendor_url>https://github.com/DankBSD/complexd</vendor_url>

	<action id="org.freedesktop.login1.inhibit-block-shutdown">
		<description>Inhibit system shutdown</description>
		<message>Inhibiting system shutdown requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-delay-shutdown">
		<description>Delay system shutdown</description>
		<message>Delaying system shutdown requires authentication.</message>
		<defaults>
			<allow_any>yes</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-block-sleep">
		<description>Inhibit system sleep</description>
		<message>Inhibiting system sleep requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-delay-sleep">
		<description>Delay system sleep</description>
		<message>Delaying system sleep requires authentication.</message>
		<defaults>
			<allow_any>yes</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-block-idle">
		<description>Block idle auto-suspend</description>
		<message>Blocking idle auto-suspend requires authentication.</message>
		<defaults>
			<allow_any>yes</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-handle-power-key">
		<description>Inhibit power button action</description>
		<message>Inhibiting power button action requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-handle-suspend-key">
		<description>Inhibit suspend button action</description>
		<message>Inhibiting suspend button action requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-handle-hibernate-key">
		<description>Inhibit hibernate button action</description>
		<message>Inhibiting hibernate button action requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-handle-lid-switch">
		<description>Inhibit laptop lid action</description>
		<message>Inhibiting laptop lid action requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.inhibit-handle-reboot-key">
		<description>Inhibit reboot button action</description>
		<message>Inhibiting reboot button action requires authentication.</message>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-self-linger">
		<description>Set self linger</description>
		<message>Letting programs run after logging out requires confirmation.</message>
		<defaults>
			<allow_any>yes</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-user-linger">
		<description>Set user linger</description>
		<message>Letting programs run after logging out requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.attach-device">
		<description>Allow attaching devices to seats</description>
		<message>Attaching a device to a seat requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.flush-devices">
		<description>Flush device seat attachments</description>
		<message>Resetting device seat attachments requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.power-off">
		<description>Power the system off</description>
		<message>Powering the system off requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.power-off-multiple-sessions">
		<description>Power the system off, ignore other sessions</description>
		<message>Powering the system off while other users are logged in requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.power-off-ignore-inhibit">
		<description>Power the system off, ignore inhibit</description>
		<message>Powering the system off while applications are using it requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.reboot">
		<description>Reboot the system</description>
		<message>Rebooting the system requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.reboot-multiple-sessions">
		<description>Reboot the system, ignore other sessions</description>
		<message>Rebooting the system while other users are logged in requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.reboot-ignore-inhibit">
		<description>Reboot the system, ignore inhibit</description>
		<message>Rebooting the system while applications are using it requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.halt">
		<description>Halt the system</description>
		<message>Halting the system requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.halt-multiple-sessions">
		<description>Halt the system, ignore other sessions</description>
		<message>Halting the system while other users are logged in requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.halt-ignore-inhibit">
		<description>Halt the system, ignore inhibit</description>
		<message>Halting the system while applications are using it requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.suspend">
		<description>Suspend the system</description>
		<message>Suspending the system requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.suspend-multiple-sessions">
		<description>Suspend the system, ignore other sessions</description>
		<message>Suspending the system while other users are logged in requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.suspend-ignore-inhibit">
		<description>Suspend the system, ignore inhibit</description>
		<message>Suspending the system while applications are using it requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.hibernate">
		<description>Hibernate the system</description>
		<message>Authentication is required to hibernate the system.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.hibernate-multiple-sessions">
		<description>Hibernate the system, ignore other sessions</description>
		<message>Hibernating the system while other users are logged in requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.hibernate-ignore-inhibit">
		<description>Hibernate the system, ignore inhibit</description>
		<message>Hibernating the system while applications are using it requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.manage">
		<description>Manage active sessions/users/seats</description>
		<message>Managing active sessions/users/seats requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.lock-sessions">
		<description>(Un)lock active sessions</description>
		<message>(Un)locking active sessions requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-reboot-parameter">
		<description>Set the next reboot message</description>
		<message>Setting the next reboot message requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-reboot-to-firmware-setup">
		<description>Boot to firmware setup interface</description>
		<message>Booting to firmware setup interface on the next reboot requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-reboot-to-boot-loader-menu">
		<description>Boot to the boot menu</description>
		<message>Booting to the boot menu on the next reboot requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-reboot-to-boot-loader-entry">
		<description>Boot a boot entry</description>
		<message>Booting to a selected boot entry on the next reboot requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.set-wall-message">
		<description>Set a wall message</description>
		<message>Setting a wall message requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>auth_admin_keep</allow_inactive>
			<allow_active>auth_admin_keep</allow_active>
		</defaults>
	</action>

	<action id="org.freedesktop.login1.chvt">
		<description>Switch virtual terminal</description>
		<message>Switching to a different virtual terminal requires authentication.</message>
		<defaults>
			<allow_any>auth_admin_keep</allow_any>
			<allow_inactive>yes</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
	</action>

</policyconfig>
