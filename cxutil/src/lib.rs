use std::{collections::HashMap, io};
use zbus_polkit::policykit1 as pk;

#[cfg(target_os = "freebsd")]
pub fn get_kenv(key: &std::ffi::CStr) -> Option<String> {
    extern "C" {
        fn kenv(act: libc::c_int, nam: *const libc::c_uchar, val: *mut libc::c_uchar, len: libc::c_int) -> libc::c_int;
    }
    let mut val: [libc::c_uchar; 129] = [0; 129];
    let len = unsafe {
        kenv(
            /* KENV_GET */ 0,
            key.as_ptr() as *const _,
            &mut val[0],
            /* KENV_MVALLEN + 1 */ 129,
        )
    };
    if len > 0 {
        return std::ffi::CStr::from_bytes_with_nul(&val[0..len as usize])
            .ok()
            .map(|s| s.to_string_lossy().into_owned());
    }
    None
}

#[cfg(target_os = "freebsd")]
pub fn set_priority_realtime() -> io::Result<()> {
    let mut rtp = libc::rtprio {
        type_: libc::RTP_PRIO_REALTIME,
        prio: 1,
    };
    if unsafe { libc::rtprio(libc::RTP_SET, libc::getpid(), &mut rtp) } != 0 {
        return Err(io::Error::last_os_error());
    }
    Ok(())
}

// With this process's death, the thread of prophecy is severed.
// Restore a saved memory snapshot to restore the weave of fate, or persist in the doomed world you have created.
#[cfg(target_os = "freebsd")]
pub fn set_process_essential() -> io::Result<()> {
    let mut prot = libc::PPROT_SET;
    if unsafe {
        libc::procctl(
            libc::P_PID,
            libc::getpid().into(),
            libc::PROC_SPROTECT,
            &mut prot as *mut libc::c_int as *mut _,
        )
    } != 0
    {
        return Err(io::Error::last_os_error());
    }
    Ok(())
}

// grrr https://stackoverflow.com/questions/62008185/how-do-i-apply-must-use-to-an-async-function
#[must_use = "use ? to perform the permission check"]
pub struct Allow;

pub async fn pk_check<'m>(
    authority: &'m pk::AuthorityProxy<'static>,
    hdr: &'m zbus::MessageHeader<'_>,
    interactive: bool,
    perm: &'static str,
) -> zbus::fdo::Result<Allow> {
    let result = authority
        .check_authorization(
            &pk::Subject::new_for_message_header(hdr).map_err(|e| zbus::fdo::Error::Failed(e.to_string()))?,
            perm,
            &HashMap::new(),
            if interactive {
                pk::CheckAuthorizationFlags::AllowUserInteraction.into()
            } else {
                Default::default()
            },
            "",
        )
        .await
        .map_err(|e| zbus::fdo::Error::Failed(e.to_string()))?;
    if !result.is_authorized {
        if !interactive || !result.is_challenge {
            return Err(zbus::fdo::Error::InteractiveAuthorizationRequired(
                "polkit auth required".to_string(),
            ));
        }
        return Err(zbus::fdo::Error::AccessDenied("polkit auth required".to_string()));
    }
    Ok(Allow)
}

pub async fn bus_peer_uid(conn: &zbus::Connection, unm: zbus::names::UniqueName<'static>) -> zbus::fdo::Result<u32> {
    zbus::fdo::DBusProxy::new(conn)
        .await?
        .get_connection_unix_user(zbus::names::BusName::Unique(unm))
        .await
}
